from django.db import models
from django.conf import settings


class Tips(models.Model):
    title = models.CharField(max_length=20)
    content = models.CharField(max_length=10000)
    thumbnail = models.URLField(max_length=200, null=True)
    publisher = models.CharField(max_length=200)
    date = models.DateTimeField()

    def __str__(self):
        return f'{self.title}'

class Review(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    tips = models.ForeignKey(Tips, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'


class Category(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    desc = models.CharField(max_length=255)
    tips = models.ManyToManyField(Tips)

    def __str__(self):
        return f'{self.name}'