# Generated by Django 4.1.2 on 2022-11-07 02:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tips', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tips',
            name='category',
            field=models.CharField(default='Todos', max_length=20),
        ),
    ]
