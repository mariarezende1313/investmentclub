from django.forms import ModelForm
from .models import Tips, Review
from django import forms


class TipsForm(ModelForm):
    content = forms.CharField( widget=forms.Textarea )
    class Meta:
        model = Tips
        fields = [
            'title', 
            'content',
            'thumbnail',
        ]
        labels = {
            'title': 'Título', 
            'content': 'Conteúdo',
            'thumbnail': 'URL da Thumbnail',
        }
class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = [
            'text',
        ]
        labels = {
            'text': 'Resenha',
        }