from django.urls import path

from . import views

app_name = 'tips'
urlpatterns = [
    path('', views.list_tips, name='index'),
    path('mytips/', views.list_user_tips, name='mytips'),
    path('<int:tips_id>/', views.detail_tips, name='detail'),
    path('search/', views.search_tips, name='search'), 
    path('create/', views.create_tips, name='create'),
    path('update/<int:tips_id>/', views.update_tips, name='update'),
    path('delete/<int:tips_id>/', views.delete_tips, name='delete'),
    path('<int:tips_id>/review/', views.create_review, name='review'),
    path('category/', views.CategoryListView.as_view(), name='category'),
    path('category/create', views.CategoryCreateView.as_view(), name='create-category'),
]
