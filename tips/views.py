from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from .models import Tips, Review, Category
from datetime import datetime
from django.urls import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from .forms import TipsForm, ReviewForm
# Create your views here.
def list_tips(request):
    tips_list = Tips.objects.all()
    context = {'tips_list': tips_list}
    return render(request, 'tips/index.html', context)

@login_required
def list_user_tips(request):
    tips_list = Tips.objects.filter(publisher=request.user)
    context = {'tips_list': tips_list}
    return render(request, 'tips/mytips.html', context)

def detail_tips(request, tips_id):
    tips = get_object_or_404(Tips, pk=tips_id)
    context = {'tips': tips}
    return render(request, 'tips/detail.html', context)

def search_tips(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        tips_list = Tips.objects.filter(title__icontains=search_term)
        context = {"tips_list": tips_list}
    return render(request, 'tips/search.html', context)

@login_required
def create_tips(request):
    if request.method == 'POST':
        form = TipsForm(request.POST)
        if form.is_valid():
            title = request.POST['title']
            content = request.POST['content']
            thumbnail = request.POST['thumbnail']
            date = datetime.now()
            publisher = request.user
            tips = Tips(title = title,
                        content = content,
                        thumbnail = thumbnail,
                        date = date, 
                        publisher = publisher)
            tips.save()
            return HttpResponseRedirect(
                reverse('tips:detail', args=(tips.id, )))
    else:
        form = TipsForm()
    context = {'form': form}
    return render(request, 'tips/create.html', context)

@login_required
def update_tips(request, tips_id):
    tips = get_object_or_404(Tips, pk=tips_id)

    if request.method == "POST":
        form = TipsForm(request.POST)
        if form.is_valid():
            title = request.POST['title']
            content = request.POST['content']
            thumbnail = request.POST['thumbnail']
            date = request.POST['thumbnail']
            publisher = request.user
            tips = Tips(title = title,
                        content = content,
                        thumbnail = thumbnail,
                        date = date, 
                        publisher = publisher)
            return HttpResponseRedirect(
                reverse('tips:detail', args=(tips.id, )))
    else:
        form = TipsForm(
            initial={
                'title': tips.title,
                'content': tips.content,
                'thumbnail': tips.thumbnail
            }
        )
    context = {'tips': tips, 'form': form}
    return render(request, 'tips/update.html', context)

@login_required
def delete_tips(request, tips_id):
    tips = get_object_or_404(Tips, pk=tips_id)

    if request.method == "POST":
        tips.delete()
        return HttpResponseRedirect(reverse('tips:index'))

    context = {'tips': tips}
    return render(request, 'tips/delete.html', context)
@login_required
def create_review(request, tips_id):
    tips = get_object_or_404(Tips, pk=tips_id)
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review_author = request.user
            review_text = form.cleaned_data['text']
            review = Review(author=review_author,
                            text=review_text,
                            tips=tips)
            review.save()
            return HttpResponseRedirect(
                reverse('tips:detail', args=(tips_id, )))
    else:
        form = ReviewForm()
    context = {'form': form, 'tips': tips}
    return render(request, 'tips/review.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'tips/category.html'


class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'tips/create-category.html'
    fields = ['name', 'author', 'desc','tips']
    success_url = reverse_lazy('tips:category')