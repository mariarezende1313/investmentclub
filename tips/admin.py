from django.contrib import admin

from .models import Tips, Review, Category

admin.site.register(Tips)
admin.site.register(Review)
admin.site.register(Category)