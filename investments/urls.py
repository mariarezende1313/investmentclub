from django.urls import path

from . import views

app_name = 'investments'
urlpatterns = [
    path('', views.InvestmentListView.as_view(), name='index'),
    path('<int:investment_id>/', views.detail_investment, name='detail'),
    path('myinvestments/', views.list_user_investments, name='myinvestments'),
    path('search/', views.search_investments, name='search'),
    path('create/', views.create_investment, name='create'),
    path('delete/<int:investment_id>/', views.delete_investment, name='delete'),
    path('update/<int:investment_id>/', views.update_investment, name='update'),
]