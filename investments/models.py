from django.db import models

class Investment(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=20, default = "XXXX")
    thumbnail = models.URLField(max_length=200, null=True)
    publisher = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.name} ({self.code})'