from django.forms import ModelForm
from .models import Investment


class InvestmentForm(ModelForm):
    class Meta:
        model = Investment
        fields = [
            'name', 
            'code',
            'thumbnail',
        ]
        labels = {
            'name': 'Nome da ação (Yahoo Finance)', 
            'code': 'Código da ação',
            'thumbnail': 'URL da Thumbnail',
        }