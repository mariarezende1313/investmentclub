import pandas as pd
import yfinance as yf
from datetime import timedelta, date
from functools import reduce
import operator

Start_date = date.today() - timedelta(days=60)
End_date = date.today()

def dataframe_30(ticker):
    data = pd.DataFrame(yf.download(ticker, period='30d',interval='1d'))
    fixed = data.tail(30)['Adj Close']
    listed = fixed.values.tolist()
    dates = data.tail(30).index.strftime("%d/%m").tolist()
    return listed, dates

def pct_increase(ticker):
    DF = pd.DataFrame(yf.download(ticker, period='30d',interval='1d')['Adj Close'])
    Fixed = DF.tail(30)
    Start = Fixed.iloc[0]
    Final = Fixed.iloc[-1]
    Sum = ((Final/Start)*100)-100
    return Sum.round(2)[0]