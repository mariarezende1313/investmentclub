from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .forms import InvestmentForm
from .models import Investment
from .data import *
from django.contrib.auth.decorators import login_required
# Create your views here.
class InvestmentListView(generic.ListView):
    model = Investment
    template_name = 'investments/index.html'

@login_required
def list_user_investments(request):
    investments_list = Investment.objects.filter(publisher=request.user)
    context = {'investments_list': investments_list}
    return render(request, 'investments/myinvestments.html', context)

@login_required
def create_investment(request):
    if request.method == 'POST':
        form = InvestmentForm(request.POST)
        if form.is_valid():
            name = request.POST['name']
            code = request.POST['code']
            thumbnail = request.POST['thumbnail']
            publisher = request.user
            investment = Investment(name = name,
                        code = code,
                        thumbnail = thumbnail,
                        publisher = publisher)
            investment.save()
            return HttpResponseRedirect(
                reverse('investments:detail', args=(investment.id, )))
    else:
        form = InvestmentForm()
    context = {'form': form}
    return render(request, 'investments/create.html', context)

def search_investments(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        investment_list = Investment.objects.filter(name__icontains=search_term)
        context = {"investment_list": investment_list}
    return render(request, 'investments/search.html', context)

def detail_investment(request, investment_id):
    investment = get_object_or_404(Investment, pk=investment_id)
    graph = dataframe_30(investment.code)
    x = graph[1]
    performance = pct_increase(investment.code)
    price = graph[0]
    context = {'price': price, 'x': x, 'performance': performance, 'investment': investment}
    return render(request, 'investments/detail.html', context)


@login_required
def update_investment(request, investment_id):
    investment = get_object_or_404(Investment, pk=investment_id)

    if request.method == "POST":
        form = InvestmentForm(request.POST)
        if form.is_valid():
            name = request.POST['name']
            code = request.POST['code']
            thumbnail = request.POST['thumbnail']
            publisher = request.user
            investment = Investment(name = name,
                        code = code,
                        thumbnail = thumbnail,
                        publisher = publisher)
            investment.save()
            return HttpResponseRedirect(
                reverse('investments:detail', args=(investment.id, )))
    else:
        form = InvestmentForm(
            initial ={
                'name': investment.name,
                'code': investment.code,
                'thumbnail': investment.thumbnail
            }
        )
    context = {'investment': investment, 'form': form}
    return render(request, 'investments/update.html', context)

@login_required
def delete_investment(request, investment_id):
    investment = get_object_or_404(Investment, pk=investment_id)

    if request.method == "POST":
        investment.delete()
        return HttpResponseRedirect(reverse('investments:index'))

    context = {'investment': investment}
    return render(request, 'investments/delete.html', context)